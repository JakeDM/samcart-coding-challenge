import nock from 'nock'
import { describe, it, before } from 'mocha'
import { expect } from 'chai'
import CarService from '../service/carservice'
import { SERVICE_HOST } from '../config'

const singleCarResp = {
  id: 'dku43920s',
  price: 4499000,
  mileage: 1,
  category: 'SUV',
  year: 2022,
  color: 'Burnt Orange',
  package: 'Badlands',
  model: 'Bronco',
  make: 'Ford',
}

const allCarsResp = [
  {
    id: 'JHk290Xj',
    price: 1999900,
    mileage: 120123,
    category: 'Truck',
    year: 2010,
    color: 'Silver',
    package: 'Base',
    model: 'F10',
    make: 'Ford',
  },
  {
    id: 'fWl37Ia',
    price: 2899000,
    mileage: 3999,
    category: 'Sedan',
    year: 2019,
    color: 'White',
    package: 'SE',
    model: 'Camry',
    make: 'Toyota',
  },
  {
    id: '1i3xjRllc',
    price: 2275000,
    mileage: 24001,
    category: 'SUV',
    year: 2018,
    color: 'Red',
    package: 'XSE',
    model: 'Rav4',
    make: 'Toyota',
  },
  {
    id: 'dku43920s',
    price: 4499000,
    mileage: 1,
    category: 'SUV',
    year: 2022,
    color: 'Burnt Orange',
    package: 'Badlands',
    model: 'Bronco',
    make: 'Ford',
  },
]

const carService = new CarService()

describe('getCar()', () => {
  before(() => {
    nock(SERVICE_HOST).get(`/cars/${'dku43920s'}`).reply(200, singleCarResp)
  })

  it('should return a car on valid id', async () => {
    return carService.getCar('dku43920s').then((r) => {
      expect(r.id).to.equal(singleCarResp.id)
    })
  })

  it('should return undefined on null id', async () => {
    return carService.getCar().then((r) => {
      expect(r).to.equal(undefined)
    })
  })
})

describe('getCars()', () => {
  before(() => {
    nock(SERVICE_HOST).get('/cars').reply(200, allCarsResp)
  })

  it('should return all cars', async () => {
    return carService.getCars().then((r) => {
      expect(r.length).to.equal(allCarsResp.length)
    })
  })
})
