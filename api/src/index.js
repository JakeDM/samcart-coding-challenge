import express from 'express'
import bodyParser from 'body-parser'
import log4js from 'log4js'
import { PORT, HOST } from './config'
import CarService from './service/carservice'

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

log4js.configure('log4js.json')
const logger = log4js.getLogger('app')

const carService = new CarService()

app.get('/cars', async (req, res) => {
  const cars = await carService.getCars()
  res.send(cars)
})

app.get('/cars/:id', async (req, res) => {
  const id = req.params.id
  const car = await carService.getCar(id)
  res.send(car)
})

app.listen(PORT, HOST, () => {
  logger.info('API layer started on port:', PORT, 'and host:', HOST)
})
