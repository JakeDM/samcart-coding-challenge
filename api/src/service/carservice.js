import axios from 'axios'
import { SERVICE_HOST } from '../config'
import log4js, { Logger } from 'log4js'

export default class CarService {
  constructor() {
    this.logger = log4js.getLogger(this.constructor.name)
  }

  async getCar(id) {
    if (!id) {
      return
    }
    this.logger.debug('Making a request to:', SERVICE_HOST + `/cars/${id}`)
    return axios.get(SERVICE_HOST + `/cars/${id}`).then((r) => r.data)
  }

  async getCars() {
    return axios.get(SERVICE_HOST + '/cars').then((r) => r.data)
  }
}
