# SamCart Coding Challenge

Jacob Miller's coding challenge submission. I've used sqlite3 for ease of db spinup.

## Running the Project

`cd` into the root directory and run `docker-compose up --build` to run both the service and api layer. The api layer runs on port 49811 and the service layer runs on port 49822.

## Consuming the API

Use any request tool (such as curl or postman) to make a request to `localhost:49811` using the following endpoints:

- /cars
- /cars/{id}

## Running Tests

`cd` into either the api or service layer project and run `npm test` to run the test suite.
