import { QueryTypes, Sequelize } from 'sequelize'

export const db = new Sequelize('sqlite::memory:')

const SEED_SCRIPT = `INSERT INTO Cars(id, make, model, package, color, year, category, mileage, price)
  VALUES ("JHk290Xj", "Ford", "F10", "Base", "Silver", 2010, "Truck", 120123, 1999900),
       ("fWl37Ia", "Toyota", "Camry", "SE", "White", 2019, "Sedan", 3999, 2899000),
       ("1i3xjRllc", "Toyota", "Rav4", "XSE", "Red", 2018, "SUV", 24001, 2275000),
       ("dku43920s", "Ford", "Bronco", "Badlands", "Burnt Orange", 2022, "SUV", 1, 4499000);`

const TABLE_DECLARATION = `
CREATE TABLE Cars
(
    price INTEGER,
    mileage INTEGER,
    category TEXT,
    year INTEGER,
    color TEXT,
    package TEXT,
    model TEXT,
    make TEXT,
    id TEXT
);`

export const seed = async () => {
  await db.query(TABLE_DECLARATION)
  return db.query(SEED_SCRIPT, { type: QueryTypes.INSERT })
}
