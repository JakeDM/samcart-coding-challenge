export default class CarService {
  constructor(carRepository) {
    this.carRepository = carRepository
  }

  async getCar(id) {
    if (!id) {
      return
    }

    return this.carRepository.getCar(id)
  }

  async getCars() {
    return this.carRepository.getCars()
  }
}
