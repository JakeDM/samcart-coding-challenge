import Car from '../models/car'

export default class CarRepository {
  getCar(id) {
    return Car.findOne({
      where: {
        id,
      },
    }).then((c) => (c && c.dataValues ? c.dataValues : null))
  }

  getCars() {
    return Car.findAll()
  }
}
