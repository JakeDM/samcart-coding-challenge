import { DataTypes, Model } from 'sequelize'
import { db } from '../config/dbconfig'

export default class Car extends Model {}

Car.init(
  {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    price: {
      type: DataTypes.INTEGER,
    },
    mileage: {
      type: DataTypes.INTEGER,
    },
    category: {
      type: DataTypes.STRING,
    },
    year: {
      type: DataTypes.INTEGER,
    },
    color: {
      type: DataTypes.STRING,
    },
    package: {
      type: DataTypes.STRING,
    },
    model: {
      type: DataTypes.STRING,
    },
    make: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize: db,
    modelName: 'Cars',
    timestamps: false,
  }
)
